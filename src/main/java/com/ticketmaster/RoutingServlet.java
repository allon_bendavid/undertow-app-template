package com.ticketmaster;

import com.ticketmaster.service.Response;
import com.ticketmaster.service.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

public class RoutingServlet extends HttpServlet {

    Map<String, Service> serviceMap = new HashMap<>();

    protected RoutingServlet(){
        ServiceLoader<Service> serviceLoader = ServiceLoader.load(Service.class);
        for (Service service : serviceLoader){
            serviceMap.put(service.getClass().getSimpleName().toUpperCase(), service);
        }

    }

    RoutingServlet(Map<String, Service> services){
        serviceMap = services;
    }

    @Override
    protected void doGet(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
            throws ServletException, IOException {
        try {
            final String uri = httpServletRequest.getRequestURI().substring(1).toUpperCase();
            Response response = serviceMap.get(uri).read(httpServletRequest.getParameterMap());
            respond(httpServletResponse, response);
        } catch (Exception e) {
            respond(httpServletResponse, new Response() {
                @Override
                public String getMessage() {
                    return "Method not allowed";
                }
                @Override
                public boolean isSuccess(){
                    return false;
                }
           });
        }
    }

    @Override
    protected void doPost(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
            throws ServletException, IOException {

        final String uri = httpServletRequest.getRequestURI().substring(1).toUpperCase();
        final Response response = serviceMap.get(uri).update(httpServletRequest.getParameterMap());
        respond(httpServletResponse, response);
    }

    @Override
    protected void doPut(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
            throws ServletException, IOException {

        final String uri = httpServletRequest.getRequestURI().substring(1).toUpperCase();
        final Response response = serviceMap.get(uri).create(httpServletRequest.getParameterMap());
        respond(httpServletResponse, response);
    }

    @Override
    protected void doDelete(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
            throws ServletException, IOException {

        final String uri = httpServletRequest.getRequestURI().substring(1).toUpperCase();
        final Response response = serviceMap.get(uri).delete(httpServletRequest.getParameterMap());
        respond(httpServletResponse, response);

    }

    private void respond(HttpServletResponse httpServletResponse, Response response) throws IOException {
        PrintWriter writer = httpServletResponse.getWriter();
        if (response.isSuccess()) {
            httpServletResponse.setStatus(200);
            writer.write(response.getMessage());
        } else {
            httpServletResponse.setStatus(400);
            writer.write(response.getMessage());
        }
        writer.close();
    }
}
