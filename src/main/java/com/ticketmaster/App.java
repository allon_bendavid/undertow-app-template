package com.ticketmaster;

import com.ticketmaster.monitoring.servletlogging.filter.LoggingFilter;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;

import javax.servlet.DispatcherType;
import javax.servlet.ServletException;

public class App
{

    public static void main( String[] args ) {

        try {
            DeploymentInfo servletBuilder = Servlets.deployment()
                    .setClassLoader(App.class.getClassLoader())
                    .setContextPath("/")
                    .setDeploymentName("app.war")
                    .addFilter(
                            Servlets.filter("logging-filter", LoggingFilter.class))
                    .addFilterUrlMapping("logging-filter", "/*", DispatcherType.REQUEST)
                    .addServlets(
                            Servlets.servlet("RoutingServlet", RoutingServlet.class)
                                    .addMapping("/*"));

            DeploymentManager manager = Servlets.defaultContainer().addDeployment(servletBuilder);
            manager.deploy();
            PathHandler path = Handlers.path(manager.start());

            Undertow server = Undertow.builder()
                    .addHttpListener(8080, "localhost")
                    .setHandler(path)
                    .build();
            server.start();
        } catch (ServletException se) {

        }
    }

}
