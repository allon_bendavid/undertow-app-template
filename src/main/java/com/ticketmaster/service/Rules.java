package com.ticketmaster.service;

import com.google.auto.service.AutoService;

import java.util.Map;

@AutoService(Service.class)
public class Rules extends Service {

    @Override
    public Response read(Map<String, String[]> parameters) {
        return new Response() {
            public String getMessage() {
                return "getting rules";
            }
            public boolean isSuccess(){
                return true;
            }
        };
    }
}
