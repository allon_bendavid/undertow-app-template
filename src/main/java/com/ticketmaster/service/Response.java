package com.ticketmaster.service;

public interface Response {
    boolean isSuccess();
    String getMessage();
}
