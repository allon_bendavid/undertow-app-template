package com.ticketmaster.service;

import java.util.Map;

public abstract class Service {

    private static final String BAD_REQUEST = "Bad request";

    public Response create(Map<String, String[]> parameters) {
        return new Response() {
            public String getMessage() {
                return BAD_REQUEST;
            }
            public boolean isSuccess(){
                return false;
            }
        };
    }
    public Response read(Map<String, String[]> parameters){
        return new Response() {
            public String getMessage() {
                return BAD_REQUEST;
            }
            public boolean isSuccess(){
                return false;
            }
        };
    }
    public Response update(Map<String, String[]> parameters){
        return new Response() {
            public String getMessage() {
                return BAD_REQUEST;
            }
            public boolean isSuccess(){
                return false;
            }
        };
    }
    public Response delete(Map<String, String[]> parameters){
        return new Response() {
            public String getMessage() {
                return BAD_REQUEST;
            }
            public boolean isSuccess(){
                return false;
            }
        };
    }
}
